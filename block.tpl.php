<?php
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?>">

<?php
  if ($block->module == 'block'):
    if (user_access('administer blocks')) :?>
    <div id="tabs-wrapper">
      <ul class="tabs primary">
        <li class="active">
          <a class="active" href='<?php print check_url(base_path()) ?>index.php?q=admin/build/block/configure/<?php print $block->module;?>/<?php print $block->delta;?>'>Edit</a>
        </li>
      </ul>
    </div>
    <?php 
    endif; 
  endif; 
?>

<?php if (!empty($block->subject)): ?>
  <h2><?php print $block->subject ?></h2>
<?php endif;?>

  <div class="content"><?php print $block->content ?></div>
</div>