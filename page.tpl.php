<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if lt IE 7]>
      <?php print phptemplate_get_ie_styles(); ?>
    <![endif]-->
  </head>
  
  <body class="<?php print $body_classes; ?>">
		<div class="whitebar">
			<div class="centered">
				<div id="slogan">
					<?php 
					if ($messages) { 
						print $messages; 
					} else if ($search_box)
					{
						print $search_box;
					}
					?>		
					<?php if ($site_slogan) { print '<h3>' . $site_slogan . '</h3>'; } ?>
				</div>
			</div>
		</div>

  	<div id="menubar">  	
			<div id="navigation">
				<?php if (isset($primary_links)) : ?>
					<?php print theme('links', $primary_links) ?>
				<?php endif; ?>
			</div>
		</div>
  
  	<div id="bluebar">
  		<div class="centered">
				<div id="logo">
					<a href="<?php print $base_path; ?>"><img src="<?php print $logo; ?>" alt="logo" /></a><?php if ($site_name) { print '<h1><a href="'. $base_path .'">'. $site_name .'</a>' . '</h1>'; } ?>
					<?php print '<h3 class="mission">' . $mission . '</h3>'; ?>
					<?php print '<div id="mission-region">' . $mission_region  . '</div>'; ?>
				</div>
			</div>
		</div>

		<?php if ($breadcrumb || $secondary_links): ?>
		<div class="whitebar">		
			<div class="centered">		
				<div id="crumbbar">
					<?php 
					if ($breadcrumb) { print '<div id="breadcrumb">' . $breadcrumb . '</div>'; } 
					if ($secondary_links) { print '<div id="secondary">' . theme('links', $secondary_links) . '</div>'; } ?>
				</div>
			</div>	
		</div>
		<?php endif; ?>

		<?php if ($callout_left || $callout_right): ?>
		<div class="whitebar">
			<div class="centered">
				<div id="callout">
					<?php if ($callout_left): ?>
					<div id="callout-left">		
						<?php	print $callout_left; ?>
					</div>  
					<?php endif; ?>
					<?php if ($callout_right) {
						print '<div id="callout-right">'.$callout_right.'</div>';
					} else 
					{
						print '<div id="callout-right-empty">&nbsp;</div>';	
					}
					?>
				</div>
			</div>
		</div>		
		<?php endif; ?>

		<div id="greybar">
			<div class="centered">
					<?php if ($tabs) { print '<div id="tabs-wrapper"><ul class="tabs primary">'. $tabs. '</ul></div>'; } ?>
					<?php 
					if ($content_left && $content_right) { print '<div id="three-content-columns">'; } 
					else if ($content_left || $content_right) { print '<div id="two-content-columns">'; }
						if ($content_left) { print '<div id="content-left">'. $content_left. '</div>'; }
						print '<div id="content">';
						if ($title) { print '<h2>'. $title . '</h2>'; }
						print $content. '</div>';
						if ($content_right) { print '<div id="content-right">'. $content_right. '</div>'; }
					if ($content_left || $content_right) { print '</div>'; }
					?>			
			</div>
		</div>
			

		<div id="lightbluebar">
			<div class="centered">
				<div id="footerinner">
					<p>Refresco designed by <a href="http://brauerranch.com">Patrick Teglia</a></p>
					<?php if($footer) { print $footer; } ?>

				</div>
		  </div>
		</div>
		
	</body>
</html>
